#!/usr/bin/perl
use Net::SMTP;
use strict;
use warnings;
####




print "-----< start $0 >-----------\n";
my $hostname = `/bin/hostname`;
chomp($hostname);
print "Hostname: $hostname\n";
my $date = `/bin/date`;
chomp($date);
print "Date: $date\n";
my $mailhost = "relay.state.vt.us";
my $maildomain = "stat.vt.us";
my $sender = "paul.haigh\@state.vt.us";
my $subject;
my $data;


#my $attachBinaryFile;
#$attachBinaryFile = '454px-Saturn_V_aerial.jpg';
my $attachBinaryFile = 'saturnv.gif';
#my $attachBinaryFile = 'RBKP-2019-02-08__18-24-54_.png';
my $attachBinaryFile2 = 'RBKP-2019-02-08__17-51-33_.gif';
my $attachTextFile;
my $capFile;

if ( $ARGV[0] ) {
     $subject = $ARGV[0];
       chomp($subject);
     $attachTextFile = $ARGV[1];
       chomp($attachTextFile);
     if($ARGV[2]){
           print "Found CAP file\n";
           $capFile = $ARGV[2];
           chomp($capFile); 
        } else {
          print "No CAP file\n";
        };
   } else {
        $subject ="Message Reporting - $hostname: Report Completed - $date";
        $data = "Message: $subject\n";
  };

use MIME::Base64 qw( encode_base64 );
#use MIME::Base64 qw( decode_base64 );
my $port;
my $recipient = "pmphaigh\@gmail.com";
my $recipient1 = "paul.haigh\@vermont.gov";
my $from = 'paul.haigh@state.vt.us';
my $to = 'paul.haigh@vermont.gov';
my $boundary = 'frontier';

my @recipients = ( "pmphaigh\@gmail.com", "paul.haigh\@vermont.gov","daniel.gwozdz\@vermont.gov" );

open(DAT, $attachTextFile) || die("Could not open text file!");
my @textFile = <DAT>;
close(DAT);

if( $ARGV[2] ){
print "Printing CapFile\n";
open(CAP,"<",$capFile) || die "Flaming death on open of $capFile: $!\n";;
$data = ();;
while (<CAP>){
 my $line = $_;
 chomp($line);
 print "$line\n";
 $data .= $line."\n";
 }
}; 

$subject = $subject." - ".$date;

my $smtp = Net::SMTP->new($mailhost, Timeout => 60) || die("Could not create SMTP object.");
print "Sending mail\n";
#$smtp->mail($from);
$smtp->mail($sender);
#$smtp->recipient($to, { SkipBad => 1 });
$smtp->recipient(@recipients, { SkipBad => 1 });  # Good
$smtp->data();
$smtp->datasend("To: $to\n");
$smtp->datasend("From: $from\n");
$smtp->datasend("Subject: $subject\n");
$smtp->datasend("MIME-Version: 1.0\n");
$smtp->datasend("Content-type: multipart/mixed;\n\tboundary=\"$boundary\"\n");
$smtp->datasend("\n");
$smtp->datasend("--$boundary\n");
$smtp->datasend("Content-type: text/plain\n");
$smtp->datasend("Content-Disposition: quoted-printable\n");
$smtp->datasend("\nToday\'s files are attached:\n");
$smtp->datasend("\n\n$data\n\n");
$smtp->datasend("\nHave a nice day! :)\n");
$smtp->datasend("--$boundary\n");
$smtp->datasend("Content-Type: application/text; name=\"$attachTextFile\"\n");
$smtp->datasend("Content-Disposition: attachment; filename=\"$attachTextFile\"\n");
$smtp->datasend("\n");
$smtp->datasend("@textFile\n");
$smtp->datasend("--$boundary\n");
$smtp->datasend("Content-Type: image/jpeg; name=\"$attachBinaryFile\"\n");
$smtp->datasend("Content-Transfer-Encoding: base64\n");
$smtp->datasend("Content-Disposition: attachment; filename=\"$attachBinaryFile\"\n");
my $buf;
open(DAT, "./$attachBinaryFile") || die("Could not open binary file!");
   binmode(DAT);
   local $/=undef;
#   while (read(DAT, my $picture, 4096)) {
   while (read(DAT, my $picture, 72*57)) {
      $buf = &encode_base64( $picture );
      $smtp->datasend($buf);
      print "Sent $attachBinaryFile\n";
   }
close(DAT);
$smtp->datasend("\n");
$smtp->datasend("--$boundary\n");
$smtp->datasend("Content-Type: image/jpeg; name=\"$attachBinaryFile2\"\n");
$smtp->datasend("Content-Transfer-Encoding: base64\n");
$smtp->datasend("Content-Disposition: attachment; filename=\"$attachBinaryFile2\"\n");
$smtp->datasend("\n");

#my $buf;
#open(DAT, "./$attachBinaryFile") || die("Could not open binary file!");
#   binmode(DAT);
#   local $/=undef;
##   while (read(DAT, my $picture, 4096)) {
#   while (read(DAT, my $picture, 72*57)) {
#      $buf = &encode_base64( $picture );
#      $smtp->datasend($buf);
#      print "Sent $attachBinaryFile\n";
#   }
#close(DAT);

my $buf2;
open(DATA, "./$attachBinaryFile2") || die("Could not open binary file!");
   binmode(DATA);
   local $/=undef;
#   while (read(DAT, my $picture, 4096)) {
   while (read(DATA, my $picture, 72*57)) {
      $buf2 = &encode_base64( $picture );
      $smtp->datasend($buf2);
   print "Sent $attachBinaryFile2\n";
   }
close(DATA);

$smtp->datasend("\n");
$smtp->datasend("--$boundary\n");
$smtp->dataend();
$smtp->quit;
print "Mail sent\n";
print "-----< end $0 | $subject >-------------\n"; 
exit;

