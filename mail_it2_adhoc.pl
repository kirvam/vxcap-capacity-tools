#!/usr/bin/perl
use Net::SMTP;
use strict;
use warnings;
####




print "-----< start $0 >-----------\n";
my $hostname = `/bin/hostname`;
chomp($hostname);
print "Hostname: $hostname\n";
my $date = `/bin/date`;
chomp($date);
print "Date: $date\n";
my $mailhost = "relay.state.vt.us";
my $maildomain = "stat.vt.us";
my $sender = "paul.haigh\@state.vt.us";
my $subject;
my $data;



my $attachBinaryFile;
# $attachBinaryFile = '454px-Saturn_V_aerial.jpg';
 $attachBinaryFile = 'saturnv.gif';
# $attachBinaryFile2 = 'RBKP-2019-02-08__17-51-33_.png';
my $attachTextFile;

# subject, text file, bin files
if ( $ARGV[0] ) {
     $subject = $ARGV[0];
       chomp($subject);
       $subject."-".$date;
       $data = "Message: $subject\n";
         } else {
         print "\n\tUsage:\n\n\tmail_2_.pl SUBJ[0] TXT or NO [1] BIN[2]\n\n";
     }  

if ( $ARGV[1] =~ m/NO/ig ){
        print "No TXT file\n";
       } else {
          $attachTextFile = $ARGV[1];
          chomp($attachTextFile);
     }

if ( $ARGV[2] ){
           print "Found BIN file\n";
           $attachBinaryFile = $ARGV[2];
           chomp($attachBinaryFile); 
        } else {
          print "No CAP file\n";
    };

use MIME::Base64 qw( encode_base64 );
my $port;
my $recipient = "pmphaigh\@gmail.com";
my $recipient1 = "paul.haigh\@vermont.gov";
my $from = 'paul.haigh@state.vt.us';
my $to = 'paul.haigh@vermont.gov';
my $boundary = 'frontier';

my @recipients = ( "pmphaigh\@gmail.com", "paul.haigh\@vermont.gov" );
my @textFile;

unless (!$attachTextFile){
open(DAT, $attachTextFile) || die("Could not open text file!");
@textFile = <DAT>;
close(DAT);

if( $attachTextFile ){
print "Printing TXTFile\n";
open(CAP,"<",$attachTextFile) || die "Flaming death on open of $attachTextFile: $!\n";;
$data = ();;
while (<CAP>){
 my $line = $_;
 chomp($line);
 print "$line\n";
 $data .= $line."\n";
  }
 }; 
};


my $smtp = Net::SMTP->new($mailhost, Timeout => 60) || die("Could not create SMTP object.");
print "Sending mail\n";
#$smtp->mail($from);
$smtp->mail($sender);
#$smtp->recipient($to, { SkipBad => 1 });
$smtp->recipient(@recipients, { SkipBad => 1 });  # Good
$smtp->data();
$smtp->datasend("To: $to\n");
$smtp->datasend("From: $from\n");
$smtp->datasend("Subject: $subject $date\n");
$smtp->datasend("MIME-Version: 1.0\n");
$smtp->datasend("Content-type: multipart/mixed;\n\tboundary=\"$boundary\"\n");
$smtp->datasend("\n");
$smtp->datasend("--$boundary\n");
$smtp->datasend("Content-type: text/plain\n");
$smtp->datasend("Content-Disposition: quoted-printable\n");
$smtp->datasend("\nADHOC files are attached:\n");
$smtp->datasend("\n\n$data\n\n");
$smtp->datasend("\n\n");
unless (!$attachTextFile){
$smtp->datasend("--$boundary\n");
$smtp->datasend("Content-Type: application/text; name=\"$attachTextFile\"\n");
$smtp->datasend("Content-Disposition: attachment; filename=\"$attachTextFile\"\n");
$smtp->datasend("\n");
$smtp->datasend("@textFile\n");
 };
$smtp->datasend("--$boundary\n");
$smtp->datasend("Content-Type: image/jpeg; name=\"$attachBinaryFile\"\n");
$smtp->datasend("Content-Transfer-Encoding: base64\n");
$smtp->datasend("Content-Disposition: attachment; filename=\"$attachBinaryFile\"\n");
#$smtp->datasend("--$boundary\n");
#$smtp->datasend("Content-Type: image/jpeg; name=\"$attachBinaryFile2\"\n");
#$smtp->datasend("Content-Transfer-Encoding: base64\n");
#$smtp->datasend("Content-Disposition: attachment; filename=\"$attachBinaryFile2\"\n");
$smtp->datasend("\n");

my $buf;
open(DAT, "./$attachBinaryFile") || die("Could not open binary file!");
   binmode(DAT);
   local $/=undef;
#   while (read(DAT, my $picture, 4096)) {
   while (read(DAT, my $picture, 72*57)) {
      $buf = &encode_base64( $picture );
      $smtp->datasend($buf);
   }
close(DAT);


$smtp->datasend("\n");
$smtp->datasend("--$boundary\n");
$smtp->dataend();
$smtp->quit;
print "Mail sent\n";
print "-----< end $0 >-------------\n"; 
exit;

