use strict;
use warnings;
use Data::Dumper;


#
#               DESC              COUNT                CPU             MEMORY               DISK               USED
#               USED                508               1343            3813248    216639589148559    128400910318024
#                OPS                  0                717            1638400     39321600000000     39321600000000
#        TOTALS-USED                508               2060            5451648    255961189148559    167722510318024
#                CAP                  0               3584            8192000    196608000000000    196608000000000
#              DELTA                508               1524            2740352    -59353189148559     28885489681976
#    PERCENTAGE-FREE                508            42.522%            33.452%           -30.189%            14.692%
#

#my $pattern = "capFile*";
#my @files = `/usr/bin/ls -1 $pattern`;
##
#my $pit = get_pit('capFile_2019-03-11__12-33-21__.dat');
#print $pit, "\n";
#exit;

my @files = list_files('capFile*');

foreach my $file ( @files ){
   chomp($file);
   print "$file\n";
};

my @list;
my($data_ref,$title) = create_data(\@files,'DESC','PERCENTAGE-FREE','CPU');
print "Dumper \$data_ref\n";
print Dumper \$data_ref;
print "\$title: $title\n";

my($date) =get_date();
my($file) = graph_it_bars($date,$data_ref,$title);
push @list, $file;

my($data_ref,$title) = create_data(\@files,'DESC','PERCENTAGE-FREE','MEMORY');
print "\$title: $title\n";
my($date) =get_date();
my($file) = graph_it_bars($date,$data_ref,$title);
push @list, $file;

my($data_ref,$title) = create_data(\@files,'DESC','PERCENTAGE-FREE','DISK');
print "\$title: $title\n";
my($date) =get_date();
my($file) = graph_it_bars($date,$data_ref,$title);
push @list, $file;

foreach my $item ( @list ){
  print $item,"\n";
}

#Subs
#
sub get_pit {
my($file) = @_;
my($cap,$day,$time,$ext) = split(/__?/,$file);
my $pit = $day."_".$time;
print $pit,"\n";
return($pit);
};


sub create_data {
my($files_ref,$headings,$type,$val) = @_;
my($title) = $val;
print "HEADING: $headings\n";
print "TYPE: $type\n";
print "VAL: $val\n";
print "TITLE: $val\n";
my(@legend,@data);
my %hash =();
foreach my $file ( @{ $files_ref } ){
   chomp($file);
   print "$file\n";
   # point_in_time = pit;
   my($pit) = get_pit($file); 
   push @legend, $pit;
   open ( my $fh, "<", $file ) || die "Flaming death on open of $file: $!\n";
   while(<$fh>){
     my $line = $_;
     chomp($line);
     print "\$line: $line\n";
     if( $line =~ m/^(#).*/){ print "Comment $1 found. -Next.\n"; next;}
     if( $line =~ m/($headings)/ig ){
          print "$1 found!\n";
          my @array = split(/\s\s*/,$line);
            foreach my $item ( 0 .. $#array ){
                  print "making hash entry: $array[$item] => $item\n";
                  $hash{$array[$item]} = $item; 
                };
               print "Dumper \%hash\n";
               print Dumper \%hash;
         };
     ##
     if( $line =~ m/($type)/ig ){ 
       print "Found: $1\n";
       print "TYPE \$line: $line\n";
       my @array = split(/\s\s*/,$line);
       my $datum = $array[$hash{$val}];
       print "value: $datum\n";
       push @data, $datum;
     };  
   };
 };
my(@data_set) = ( [ @legend ] , [ @data ] );
return(\@data_set,$title);
};

sub list_files {
my($pattern) = @_;
my @files = `/usr/bin/ls -1 $pattern`;
return (@files);
};

sub graph_it_bars {
my($date,$data,$title) = @_;
my $file_png;
my(@legend,@data);
use GD::Graph::bars;
use GD::Graph::lines;
use GD::Graph::area;
use GD::Text;
my $graph = GD::Graph::lines->new(800,800);
#my $graph = GD::Graph::area->new(800,800);
#my $graph = GD::Graph::bars->new(800,800);
my $file_title = $title;
$title = $title." % Capacity Over Time";
my $t2 = "Used vs. Free";
$graph->set(
    dclrs => [ qw(red green) ], 
    x_label           => 'Days',
    y_label           => '% FREE, Negative is Over-Prescibed',
    title             => $title,
   # '3d'                => 0,
    y_max_value       => 100,
    y_min_value       => 0,
    #y_plot_values     => 1,
    y_tick_number     =>20,
    #y_label_skip      => 4,
    x_labels_vertical =>1, 
    #label => $t2,
    ) or die $graph->error;
if( $title =~ m/DISK/ig ) { $graph->set( y_min_value       => -100, y_tick_number     => 40 ) or die $graph->error; }
#  my @my_data = ( [ @legend ] , [ @data ] );
   my @my_data = ( @{ $data } );
      print "Dumper \@my_data\n";
      print Dumper \@my_data;
      my $gd = $graph->plot( \@my_data ) or die $graph->error;
      # make PNG
        $file_png = "CAPH-".$file_title."-".$date.".png";
        open(IMG, '>',$file_png) or die $!;
        binmode IMG;
        print IMG $gd->png;
        close IMG;
        print "Created file: $file_png\n";
       
 print "\nFinished with bar graph creation!!!\n\n";
 return($file_png);
};

sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
$mon = sprintf("%02d", $mon);
$sec = sprintf("%02d", $sec);
$min = sprintf("%02d", $min);
$hour = sprintf("%02d", $hour);
my $date = $year."-".$mon."-".$mday."__".$hour."-".$min."-".$sec."_";
return ($date);
};


