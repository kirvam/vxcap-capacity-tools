#!/usr/bin/perl
# AUTH: PMH
# DATE: 03012019
# VERS: 1.0
# NOTE: 
#   Script to report on all servers in the VxRack and produce Capacity Graphs including CPU, MEMORY, and DISK.
#
use strict;
use warnings;
use Data::Dumper;
use POSIX;
#use mapper;

####
# Current VxRack Nodes:
my @host_list = (
"eagpvxnode01 224 512000 12288000000000",
"eagpvxnode02 224 512000 12288000000000",
"eagpvxnode03 224 512000 12288000000000",
"eagpvxnode04 224 512000 12288000000000",
"eagpvxnode05 224 512000 12288000000000",
"eagpvxnode06 224 512000 12288000000000",
"eagpvxnode07 224 512000 12288000000000",
"eagpvxnode08 224 512000 12288000000000",
"eagpvxnode09 224 512000 12288000000000",
"eagpvxnode10 224 512000 12288000000000",
"eagpvxnode11 224 512000 12288000000000",
"eagpvxnode12 224 512000 12288000000000",
"eagpvxnode13 224 512000 12288000000000",
"eagpvxnode14 224 512000 12288000000000",
"eagpvxnode15 224 512000 12288000000000",
"eagpvxnode16 224 512000 12288000000000"
);
####
# Operational Buffer, roughly 20% of Hosts
my @operation_buffer = (
"eagpvxnode14 224 512000 12288000000000",
"eagpvxnode15 224 512000 12288000000000",
"eagpvxnode16 224 512000 12288000000000"
);

my $cmd_file = shift;
my %main;
my $date;
my $pwd = "/home/phaigh/vmware-vsphere-cli-distrib/vm_scripts";
my $what_is_pwd = `pwd`;
chomp($what_is_pwd);
print "\$what_is_pwd: $what_is_pwd\n";
`cd $pwd`;
my $what_is_pwd_again = `pwd`;
chomp($what_is_pwd_again);
print "\$what_is_pwd_again: $what_is_pwd_again\n";
print "\$pwd: $pwd\n";
##################################################################
# Create CAP/CAPACITY entry into %main
my($host_href,$total_href,$total_with_key_href) = make_hash(\@host_list,'CAP',);
my($main_href) = add_to_hash(\%main,$total_with_key_href);
#print "\$total_href:\n";
#print Dumper \$total_href;
#print "\$main_href:\n";
#print Dumper \$main_href;

my($buffer_ref) = find_buffer($total_with_key_href,'OPS');
#print "\$buffer_ref:\n";
#print Dumper \$buffer_ref;

my($main_href) = add_to_hash(\%main,$buffer_ref);
#print "\$main_href:\n";
#print Dumper \$main_href;

#my $help = shift;
if ( $cmd_file =~ m/^help*/ig ){
  print "\n\n######################################################\nHelp: \n\t$0 VxCAp.in(cmd file)\n\n\t This is a special command runner which extracts and calclates VxRack Capacity using an external cmd file.\n\n";
  exit;
}

$date = get_date();
print "---< Start $0 :  $date >----\n";
# This is the main driver of the script
read_table($pwd,$date,$cmd_file);

my($date) = get_date();
print "-----< end : $date >----\n";


sub read_table{
my($pwd,$date,$cmd_file) = @_;
print "------< read_table:  >------\n";
open ( DATA, "<", $cmd_file ) || die "Flaming death on open of file: $cmd_file: $!\n";
while(<DATA>){
my $line = $_;
chomp($line);
my($tag,$log_file_name,$suffix,$cmd) = split(/\|/,$line);
$log_file_name = make_file_name($log_file_name,$suffix,$date);
my $capFile = make_file_name('capFile','dat',$date);
print "-------------< running $tag >-------\n";
print "\t\t\tlog_file_name:   $log_file_name\n";
my @array = split(/\s\s*/,$cmd);
print "\t\t\tcmd:   $array[0] $array[1]\n";
my $repfile;
my($exit_val,$repfile) = cmd_runner($cmd,$log_file_name);
###print "exit_val: $exit_val\n";
###print "logfile repfile: $repfile\n";
#exit;
# CAN DEBUG BY PLUGGING REPFILE
#my $repfile = "pc_get_used__2019-02-01__13-01-48__.csv"; 
#my $repfile = "pc_get_used__2019-02-07__16-57-00__.csv";
print "logfile repfile: $repfile\n";
my($AoA_ref,$host_hash_ref,$totals_hash_ref,$totals_hash_ref_with_keys) = scan_rep($repfile,'USED');
my($main_href) = add_to_hash(\%main,$totals_hash_ref_with_keys);

print "Dumper \$totals_hash_ref_with_keys  USED ## should have key ###\n";
print Dumper \$totals_hash_ref_with_keys;
print "Dumper \$total_href  CAP\n";
print Dumper \$total_href;
print "Dumper \$buffer_ref  OPS\n";
print Dumper \$buffer_ref;
print "Dumper \$main_href  MAIN\n";
print Dumper \$main_href;
my @add_these = qw ( USED OPS );
my($total_ref,$plain_total_ref) = add_these(\@add_these,\%main,'TOTALS-USED');
print "Dumper \$total_ref\n";
print Dumper \$total_ref;

my($main_href) = add_to_hash(\%main,$total_ref);
print "Dumper \$main_href  MAIN\n";
print Dumper \$main_href;
my @add_these = qw ( CAP TOTALS-USED );
print "Dumper \@add_these\n";
print Dumper \@add_these;
my($total_delta_ref,$plain_delta_total_ref) = minus_these(\@add_these,\%main,'DELTA');
my($main_href) = add_to_hash(\%main,$total_delta_ref);
print "Dumper \$main_href  MAIN\n";
print Dumper \$main_href;
print Dumper \@add_these;
### PERCENTAGES
my @percent_these = qw ( CAP DELTA );
print "Dumper \@percent_these\n";
print Dumper @percent_these;

my($total_delta_ref,$plain_delta_total_ref) = mk_percent(\@percent_these,\%main,'PERCENTAGE-FREE');
my($main_href) = add_to_hash_int(\%main,$total_delta_ref);
print "Dumper \$main_href  MAIN AFTER ADD PERCENTAGES\n";
print Dumper \$main_href;
###
my($total_delta_ref,$plain_delta_total_ref) = mk_percent_used(\@percent_these,\%main,'PERCENTAGE-USED');
my($main_href) = add_to_hash_int(\%main,$total_delta_ref);
print "Dumper \$main_href  MAIN AFTER ADD PERCENTAGES\n";
print Dumper \$main_href;
###
my @sort = qw ( USED OPS TOTALS-USED CAP DELTA PERCENTAGE-FREE);
my @order = qw ( COUNT CPU MEMORY DISK USED );
my($string) = sort_hash_by_key(\@sort,\@order,\%main);
print "\n\$string:\n^$string^\n\n";
###exit;
print "Dumper \$main_href  MAIN\n";
print Dumper \$main_href;

my $title = "DESC,COUNT,CPU,MEMORY,DISK,USED";
my($data,$finalFile) = make_file_with_string($title,$string,$capFile,$date);
print "printing Dumper \$data\n";
print Dumper \$data;
print "capFile: $capFile\n";
#$repfile = $pwd."/".$repfile;
print "repfile: $repfile\n";
# exit;
##$date = get_date();

###
##my $date = get_date();
print "/Here is \$date: $date\n";
my @array = qw ( CPU MEMORY );
my($data) = mk_graph_data(\%main,'PERCENTAGE-FREE',\@array);
#print "Dumper \$data\n";
##print Dumper \$data;
graph_it_pies($date,$data);


print "\n";
my @array = qw ( DISK );
my($data) = mk_graph_data(\%main,'PERCENTAGE-FREE',\@array);
#print "Dumper \$data\n";
#print Dumper \$data;
my($disk_file) = graph_it_bars($date,$data);
print "DISK FILE: $disk_file\n";

my @array = qw ( CPU );
my($data) = mk_graph_data(\%main,'PERCENTAGE-FREE',\@array);
#print "Dumper \$data\n";
#print Dumper \$data;
my($cpu_file) = graph_it_pies($date,$data);
print "CPU FILE: $cpu_file\n";

my @array = qw ( MEMORY  );
my($data) = mk_graph_data(\%main,'PERCENTAGE-FREE',\@array);
#print "Dumper \$data\n";
#print Dumper \$data;
my($memory_file) = graph_it_pies($date,$data);
print "MEMORY FILE: $memory_file\n";

#exit;


print "\$date: $date\n\n";

###
my $email_log_file = "email-out_".$date."_.out";
print "email_log_file: $email_log_file\n";
#my($exit_val,$logfile) = cmd_runner("/home/phaigh/vmware-vsphere-cli-distrib/vm_scripts/fix2/mail_it2_attach_working_multi.pl \"VxRack Server List and Capacity Report\" $repfile $capFile",$email_log_file);

my($exit_val,$logfile) = cmd_runner("./mail_it2_attach_working_multi_NEW.pl \"VxRack Server List and Capacity Report\" $repfile $capFile $cpu_file $memory_file $disk_file",$email_log_file);


  print "exit_val: $exit_val\n";
  print "logfile: $logfile\n";

 };
};
###
### SUBS
###
sub mk_graph_data{
my($href,$data_set_name,$aref)  = @_;
my(@data);
foreach my $key ( @{ $aref } ){
  my $val = ${$href}{$data_set_name}{$key};
  $val =~ s/\%//;
  my $diff = 100 - $val;
  print "$key: 100% \- $val\%\[free] = $diff\%\[used\]\n";
  push @data, "$key,100,$val,$diff";
 };
return(\@data);
};

sub graph_it_bars {
my($date,$data) = @_;
my $file_png;
my @files;
my $array_count = $#{ $data };
print "\$array_count: $array_count\n";
foreach my $ii ( 0 .. $#{ $data } ){
my $line = @{$data}[$ii];
print "\$line: $line\n";
my($title,$total,$free,$used) = split(/,/,$line);
my(@legend,@data);
my $file_title = $title;
push @legend,  $free." FREE";
push @legend, $used." USED";
push @data, $free;
push @data, $used;

use GD::Graph::bars;
use GD::Text;
my $graph = GD::Graph::bars->new(300,300);
$title = $title." Capacity Graph";
my $t2 = "Used vs. Free";
$graph->set(
    x_label           => 'Over 100% is Over-Prescribed',
    y_label           => '% FREE or USED',
    title             => $title,
    y_max_value       => 250,
    y_tick_number     => 50,
    y_label_skip      => 25,
   ) or die $graph->error;
      my @my_data = ( [ @legend ] , [ @data ] );
      print "Dumper \@my_data\n";
      print Dumper \@my_data;
      my $gd = $graph->plot( \@my_data ) or die $graph->error;
      # make PNG
      $file_png = "CAP-".$file_title."-".$date.".png";
       open(IMG, '>',$file_png) or die $!;
         binmode IMG;
         print IMG $gd->png;
         close IMG;
         print "Created file: $file_png\n";
         push @files, $file_png;
        };
        print "\nFinished with bar graph creation!!!\n\n";
     if ( $array_count > 0 ) {
         return(\@files)
          } else {
           print "Only one Entry, returning single file: $file_png\n";
           return($file_png);
   }
};
  
sub graph_it_pies {
my($date,$data) = @_;
my $file_png;
my @files;
my $array_count = $#{ $data };
print "\$array_count: $array_count\n";
foreach my $ii ( 0 .. $#{ $data } ){
my $line = @{$data}[$ii];
print "\$line: $line\n";
my($title,$total,$free,$used) = split(/,/,$line);
my(@legend,@data);
my $file_title = $title;
push @legend,  $free." FREE";
push @legend, $used." USED";
push @data, $free;
push @data, $used;

use GD::Graph::pie;
use GD::Text;
my $graph = GD::Graph::pie->new(300,300);
$title = $title." Capacity Graph";
my $t2 = "Used vs. Free";
$graph->set(
   # x_label           => 'Samples taken over Time',
   # y_label           => 'GB\s Capacity Used on the Rubriks',
   # title             => 'CPU Capacity Graph',
     title             => $title,
    '3d'                => 0,
    label => $t2,
     ) or die $graph->error;
        my @my_pie_data = ( [ @legend ] , [ @data ] );
        print "Dumper \@my_pie_data\n";
        print Dumper \@my_pie_data;
   
        my $gd = $graph->plot( \@my_pie_data ) or die $graph->error;
        # make PNG
        $file_png = "CAP-".$file_title."-".$date.".png";
         open(IMG, '>',$file_png) or die $!;
           binmode IMG;
           print IMG $gd->png;
           close IMG;
           print "Created file: $file_png\n";
           push @files, $file_png;
          };
          print "\nFinished with pie graph creation!!!\n\n";
         if ( $array_count > 0 ) {
              return(\@files)
             } else {
              print "Only one Entry, returning single file: $file_png\n";
              return($file_png);
   }
};
   #                   
###
sub add_to_hash_int {
print "-----------< add_to_hash_int >-----------------\n";
my($main_href,$add_href) = @_;
foreach my $key ( keys %{ $add_href } ){
       print $key,"\n";
        foreach my $ii ( keys ${$add_href}{$key} ){
           print "Adding this: ";
           print $ii," => ";
           my $val = ${$add_href}{$key}{$ii};
           print ${$add_href}{$key}{$ii};
           print "|  \$val: $val\n";
           my $val = ${$add_href}{$key}{$ii};
           #$val = sprintf("%d.2f%%",$val);
           # sprintf("%.2f%%", $chg);
           ${$main_href}{$key}{$ii} = $val;
           print "$val added = \${\$main_href}{$key}{$ii}: ${$main_href}{$key}{$ii}\n";
          }
   };
 print "Dumper after adding PERCENTAGES!!!\n";
 print Dumper \$main_href;
print "-----------< add_to_hash_int >-----------------\n";
 return(\$main_href);
};

sub add_to_hash {
my($main_href,$add_href) = @_;
foreach my $key ( keys %{ $add_href } ){
       print $key,"\n";
        foreach my $ii ( keys ${$add_href}{$key} ){
           print $ii," => ";
           print ${$add_href}{$key}{$ii},"\n";
           my $val = ${$add_href}{$key}{$ii};
           $val = ceil($val);
           ${$main_href}{$key}{$ii} = $val;
          }
    };
 #print Dumper \$main_href;
 return(\$main_href);
};

sub sort_hash_by_key {
my($sort_aref,$order_aref,$href) = @_;
my $string;
foreach my $key ( 0 .. $#{ $sort_aref } ){
      print ${$sort_aref}[$key],"\n";
      $string .= ${$sort_aref}[$key].",";
 # foreach my $item ( keys ${$href}{${$sort_aref}[$key]} ){
 foreach my $item ( 0 .. $#{ $order_aref } ){
      $item = ${$order_aref}[$item];
      my $tmp = $item;
      print "$tmp\n";
      print "$item => ${$href}{${$sort_aref}[$key]}{$tmp}\n";
      $string .= "${$href}{${$sort_aref}[$key]}{$tmp},";
   };
      print "\n";
      $string .= "\n";
 }
 print "\n$string\n";
 return($string);
};

###
sub mk_percent_used {
print "-------< mk percent >-----\n";
my($aref,$href,$title) = @_;
my %totals;
my %plain_totals;
if ( ! $title ) { $title = "PERCENT-USED" };
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  ${$href}{$tmp} ){
         print "^$ii^ => ${$href}{$tmp}{$ii}\n";
         if( $ii =~ /^COUNT$/i ){ print "### matches COUNT!!!\n"; };
         #if( $totals{$title}{$ii} eq 0 ){ print "found zero value: $totals{$title}{$ii}\n"; };
         #if( ${$href}{$tmp}->{$ii} eq 0 ){ print "found zero value: ${$href}{$tmp}->{$ii}\n"; };
        if( $tag ge 1 && $ii !~ m/COUNT/ ){
        print "$totals{$title}{$ii} eq ${$href}{$tmp}->{$ii} div $totals{$title}{$ii}\n";
        $totals{$title}{$ii} =  int (${$href}{$tmp}->{$ii}) / int ($totals{$title}{$ii});
         print "=> $totals{$title}{$ii}\n";
         my $val = sprintf( "%.3f%%", 100 - ( 100 * $totals{$title}{$ii}) );
         print "=> $val, $totals{$title}{$ii} \n";
         $totals{$title}{$ii} = $val;
         #perl -e ' $chg = 2016 / 3584; print $chg."\n"; $per = sprintf( "%.2f%%", $chg); print "$per\n"; '
        $plain_totals{$ii} =  int (${$href}{$tmp}->{$ii}) / int($plain_totals{$ii}) ;
          } else {
        $totals{$title}{$ii} = ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} = ${$href}{$tmp}->{$ii};
      };         
  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print "Dumper \%plain_totals\n";
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};
###
sub mk_percent {
print "-------< mk percent >-----\n";
my($aref,$href,$title) = @_;
my %totals;
my %plain_totals;
if ( ! $title ) { $title = "PERCENT-FREE" };
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  ${$href}{$tmp} ){
         print "^$ii^ => ${$href}{$tmp}{$ii}\n";
         if( $ii =~ /^COUNT$/i ){ print "### matches COUNT!!!\n"; };
         #if( $totals{$title}{$ii} eq 0 ){ print "found zero value: $totals{$title}{$ii}\n"; };
         #if( ${$href}{$tmp}->{$ii} eq 0 ){ print "found zero value: ${$href}{$tmp}->{$ii}\n"; };
        if( $tag ge 1 && $ii !~ m/COUNT/ ){
        print "$totals{$title}{$ii} eq ${$href}{$tmp}->{$ii} div $totals{$title}{$ii}\n";
        $totals{$title}{$ii} =  int (${$href}{$tmp}->{$ii}) / int ($totals{$title}{$ii});
         print "=> $totals{$title}{$ii}\n";
         my $val = sprintf( "%.3f%%", 100 * $totals{$title}{$ii} );
         print "=> $val, $totals{$title}{$ii} \n";
         $totals{$title}{$ii} = $val;
         #perl -e ' $chg = 2016 / 3584; print $chg."\n"; $per = sprintf( "%.2f%%", $chg); print "$per\n"; '
        $plain_totals{$ii} =  int (${$href}{$tmp}->{$ii}) / int($plain_totals{$ii}) ;
          } else {
        $totals{$title}{$ii} = ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} = ${$href}{$tmp}->{$ii};
      };         
  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print "Dumper \%plain_totals\n";
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};

sub minus_these {
print "-------< minus these >-----\n";
my($aref,$href,$title) = @_;
my %totals;
my %plain_totals;
if ( ! $title ) { $title = "DELTA" };
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  ${$href}{$tmp} ){
         print "^$ii^ => ${$href}{$tmp}{$ii}\n";
         if( $ii =~ /^COUNT$/i ){ print "### matches COUNT!!!\n"; };
        if( $tag ge 1 && $ii !~ m/COUNT/ ){
        $totals{$title}{$ii} -= ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} -= ${$href}{$tmp}->{$ii};
          } else {
        $totals{$title}{$ii} = ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} = ${$href}{$tmp}->{$ii};
      };         
  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print "Dumper \%plain_totals\n";
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};

sub add_these {
my($aref,$href,$title) = @_;
my %totals;
my %plain_totals;
if ( ! $title ) { $title = "TOTAL-USED" };
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  ${$href}{$tmp} ){
        print "$ii => ${$href}{$tmp}{$ii}\n";
        $totals{$title}{$ii} += ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} += ${$href}{$tmp}->{$ii};

  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print "Dumper \%plain_totals\n";
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};

###
sub make_file_with_string {
print "---< make_array_string >---\n";
my($title,$string,$capFile,$date) = @_;
my $string = $title."\n".$string;
my @data = split(/\n/,$string);
print "Dumper \@data\n";
print Dumper \@data;
my $aref = \@data;
open( FH, ">",$capFile ) || die "Flaming death on open of $capFile: $!\n";
print FH "# CAPFILE: $date\n";
foreach my $item ( @{ $aref } ){
   #print "$item\n";
   my @array = split(/,\s*/,$item);
    foreach my $ii ( 0 .. $#array ){
        #print "\$ii: $ii: $array[$ii]\n";
        #print "------------< make_file_with_string >------------------\n";
        printf '%19s', $array[$ii];
        printf FH '%19s', $array[$ii];     
       $string .= sprintf ('%19s', $array[$ii].",");
      }
   print FH "\n";
   print "\n";
   $string .= "\n"; 
 };
   print FH "# CAPFILE: $date\n";
 close FH;
 return(\$string,\$capFile);
};

sub make_array_string_file {
print "---< make_array_string >---\n";
my($aref,$capFile) = @_;
my $string = "\n";
open( FH, ">",$capFile ) || die "Flaming death on open of $capFile: $!\n";
foreach my $item ( @{ $aref } ){
   #print "$item\n";
   my @array = split(/,\s+/,$item);
    foreach my $ii ( 0 .. $#array ){
        #print "\$ii: $ii: $array[$ii]\n";
        printf '%19s', $array[$ii];
        printf FH '%19s', $array[$ii];     
       $string .= sprintf ('%19s', $array[$ii].",");
      }
   print FH "\n";
   print "\n";
   $string .= "\n"; 
 };
 close FH;
 return(\$string,\$capFile);
};

# not used
sub dump_to_file {
my($aref,$file) = @_;
open( my $fh, ">", $file ) || die "Flaming death on open of $file: $!\n";
print $fh "\n\n Private Cloud VxRack Capacity Email:\n";
print "\n\n Private Cloud VxRack Capacity Email:\n";

foreach my $item ( @{ $aref } ){
   print "$item\n";
   print $fh "$item\n";
 }
close($fh);
};

###
sub compare_hashes {
print "---< compare >----\n";
my($href1,$title1,$href2,$title2) = @_;
print Dumper \$href1;
my @data;
my $string = "TITLE, VMS, CPU, MEMORY, DISK, USED";
print $string,"\n";
push @data, $string;
my @map = qw ( COUNT CPU MEMORY DISK USED );
my $number;
my $delta;
 print "$title1, ";
 print ${$href1}{'CPU'},"\n";
 $string = "$title1, ";
     foreach my $val ( 0 .. $#map ){
          print "$val\n";
          print "$map[$val]\n";
          print ${$href1}{$map[$val]},"\n";
          $number = ${$href1}{$map[$val]};
          print "$number, ";
          $string .= "$number, ";
       };
     print "\n";
     push @data, $string;
my @minus;
push @minus, "DELTA";
print "$title2, ";
my $string = "$title2, ";
     foreach my $val ( 0 .. $#map ){
          if ( $val eq 0 ){
              $number = "NA";
              $delta = " ";
            } else {
          $number = ${$href2}{$map[$val]};
          my $minus = ${$href1}{$map[$val]};
          $delta = ($number - $minus);
          }
          push @minus, $delta;
        print "$number, ";
        $string .= "$number, ";
            
       };
     print "\n";
     push @data, $string;
my $string;
foreach my $item ( @minus ){
          print "$item, ";
          $string .= "$item, ";
  };
 print "\n";
 push @data, $string;
print "-----< end >-------\n";
return(\@data);
};

sub scan_rep{
my($file,$title) = @_;
my @AoA;
my %host_hash;
my %totals_hash;
my %totals_hash_with_key;
#if(! $title ) { $title = 'USED' };
open( my $fh, "<", $file ) || die "Flaming death on open of $file: $!\n";
while(<$fh>){
my $line = $_;
chomp($line);
 print "$line\n";
 $line =~ s/\s\s*//g;
# $line =~ s/\t*//;
# print "line: $line\n";
 my @array = split(/,/,$line);
 push @AoA, [ @array ]; 
 my($ln,$name,$cpu,$mem,$prov,$used,$per) = split(/,/,$line);
  $host_hash{$name}{Name} = $name;
  $host_hash{$name}{CPU} = $cpu;
  $host_hash{$name}{MEMORY} = $mem;
  $host_hash{$name}{DISK} = $prov; # Provisioned
  $host_hash{$name}{USED} = $used;
  $host_hash{$name}{Percent} = $per;
  $host_hash{$name}{DISK} = $used;
   $totals_hash{CPU} += $cpu;
   $totals_hash{MEMORY} += $mem;
   $totals_hash{DISK} += $prov;
   $totals_hash{USED} += $used;
   $totals_hash{COUNT} = $ln; 
   $totals_hash_with_key{$title}{CPU} += $cpu;
   $totals_hash_with_key{$title}{MEMORY} += $mem;
   $totals_hash_with_key{$title}{DISK} += $prov;
   $totals_hash_with_key{$title}{USED} += $used;
   $totals_hash_with_key{$title}{COUNT} = $ln; 
 };
 print Dumper \@AoA;
 print Dumper \%host_hash;
 print "Total USED: \%totals_hash\n";
 print Dumper \%totals_hash;
 print "This is a Global\n";
 print "Total OPS BUFFER: \$buffer_ref\n";
 print Dumper \$buffer_ref;
 print "This is a GLOBAL\n";
 print "Total CAP: \$total_href\n";
 print Dumper \$total_href;
  print "Dumper \%totals_hash_with_key\n";
  print Dumper \%totals_hash_with_key;
 return(\@AoA,\%host_hash,\%totals_hash,\%totals_hash_with_key);
};

sub cmd_runner {
my $date = get_date ();
print "------------------< cmd_runner : $date >--------------------\n";
my($cmd,$logfile) = @_;
$cmd = $cmd." >".$logfile;
#print $cmd,"\n";
my @array = split(/\s\s*/,$cmd);
print "\t\t\tcmd:   $array[0] $array[1]\n";
system($cmd);
if ($? == -1) {
    print "failed to execute: $!\n";
}
elsif ($? & 127) {
    printf "child died with signal %d, %s coredump\n",
    ($? & 127),  ($? & 128) ? 'with' : 'without';
}
else {
    printf "child exited with value %d\n", $? >> 8;
 };
 my($exit_val) = $?;
 return($exit_val,$logfile);
};

sub make_file_name{
my($name,$suffix,$date) = @_;
#my $date = get_date();
print "Here is \$date: $date\n";
my $file = $name."_".$date."_.".$suffix;
print "--------< File created: $file >----\n";
return $file;
};

sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
$mon = sprintf("%02d", $mon);
$sec = sprintf("%02d", $sec);
$min = sprintf("%02d", $min);
$hour = sprintf("%02d", $hour);
my $date = $year."-".$mon."-".$mday."__".$hour."-".$min."-".$sec."_";
return ($date);
};

sub make_hash {
print "---< make_hash >---\n";
my($host_aref_list,$title) = @_;
my %host;
my %total;
my %total_with_key;
foreach my $line ( @{$host_aref_list} ){
chomp($line);
my($name,$cpu,$mem,$disk) = split(/\s/,$line);
$host{$name}{'CPU'} = $cpu;
$host{$name}{'MEMORY'} = $mem;
$host{$name}{'DISK'} = $disk;

$total{'CPU'} += $cpu;
$total{'MEMORY'} += $mem;
$total{'DISK'} += $disk;
$total{'USED'} += $disk;
  $total_with_key{$title}{'CPU'} += $cpu;
  $total_with_key{$title}{'MEMORY'} += $mem;
  $total_with_key{$title}{'DISK'} += $disk;
  $total_with_key{$title}{'USED'} += $disk;
 };
$total{'COUNT'} = "0";
$total_with_key{$title}{'COUNT'} = "0";
 
 # print Dumper \%host;
 # print Dumper \%total;
print "---< end  make_hash >---\n";
  return(\%host,\%total,\%total_with_key);
};

sub find_buffer {
my %buffer;
my($href,$title) = @_;
print Dumper \$href;
if(! $title ) { $title = 'OPS' };
foreach my $key ( keys %{ $href } ){
   print "\$key: $key\n";
    foreach my $ii ( keys ${$href}{$key} ){
    my $val = ${$href}{$key}{$ii};
     print "\tval:  $val\n";
        $val = $val * .20;
        $val = ceil($val);
        $buffer{$title}{$ii} = $val;
   }
 }
  return(\%buffer);
};


sub isanumber {
my($string) = @_;
print "---<isanumber>---\nOriginal value: $string\n";
if ( $string =~ /^(\d+\.?\d*)$/ ) {
        $string = ceil($string);
        print "$string is a number.\n";
        } else {
         print "$string NOT a number.\n";
    }
print "---<isanumber>---\n";
return($string);
};


