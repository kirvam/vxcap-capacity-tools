use strict;
use warnings;
use Data::Dumper;

my %main = (
            'OPS' => {
                       'DISK' => '39321600000000',
                       'COUNT' => 0,
                       'USED' => '39321600000000',
                       'MEMORY' => 1638400,
                       'CPU' => 717
                     },
            'TOTALS-USED' => {
                               'DISK' => '247857359281042',
                               'CPU' => 2018,
                               'MEMORY' => 5282688,
                               'USED' => '162749406860038',
                               'COUNT' => '484'
                             },
            'USED' => {
                        'DISK' => '208535759281042',
                        'CPU' => 1301,
                        'MEMORY' => 3644288,
                        'USED' => '123427806860038',
                        'COUNT' => 484
                      },
            'DELTA' => {
                         'DISK' => '-51249359281042',
                         'CPU' => '1566',
                         'MEMORY' => '2909312',
                         'USED' => '33858593139962',
                         'COUNT' => '484'
                       },
            'CAP' => {
                       'DISK' => '196608000000000',
                       'CPU' => '3584',
                       'MEMORY' => '8192000',
                       'USED' => '196608000000000',
                       'COUNT' => '0'
                     },

            'FREE' => {
                       'DISK' => '-28.443%',
                       'CPU' => '43.471%',
                       'MEMORY' => '34.602%',
                       'USED' => '15.306%',
                       'COUNT' => '491'
                     }
          );




my $date = get_date();
print "GRAPH PIES\n";
my @array = qw ( CPU MEMORY );
my($data) = mk_graph_data(\%main,'FREE',\@array);
#print "Dumper \$data\n";
#print Dumper \$data;
my(@files) = graph_it_pies($date,$data);


print "\n";
my @array = qw ( DISK );
my($data) = mk_graph_data(\%main,'FREE',\@array);
#print "Dumper \$data\n";
#print Dumper \$data;
my($disk_file) = graph_it_bars($date,$data);
print "DISK FILE: $disk_file\n";

print "GRAPH PIES\n";
my @array = qw ( CPU );
my($data) = mk_graph_data(\%main,'FREE',\@array);
#print "Dumper \$data\n";
##print Dumper \$data;
my($cpu_file) = graph_it_pies($date,$data);
print "CPU FILE: $cpu_file\n";



exit;

#################
sub mk_graph_data{
my($href,$data_set_name,$aref)  = @_;
my(@data);
foreach my $key ( @{ $aref } ){
  my $val = ${$href}{$data_set_name}{$key};
  $val =~ s/\%//;
  my $diff = 100 - $val;
  print "$key: 100% \- $val\%\[free] = $diff\%\[used\]\n";
  push @data, "$key,100,$val,$diff";
 };
return(\@data);
};


sub graph_it_bars {
my($date,$data) = @_;
my $file_png;
my @files;
my $array_count = $#{ $data };
print "\$array_count: $array_count\n";
foreach my $ii ( 0 .. $#{ $data } ){
my $line = @{$data}[$ii];
print "\$line: $line\n";
my($title,$total,$free,$used) = split(/,/,$line);
my(@legend,@data);
my $file_title = $title;
push @legend,  $free." FREE";
push @legend, $used." USED";
push @data, $free;
push @data, $used;

use GD::Graph::bars;
use GD::Text;
my $graph = GD::Graph::bars->new(300,300);
$title = $title." Capacity Graph";
my $t2 = "Used vs. Free";
$graph->set(
    x_label           => 'Over 100% is Over-Prescribed',
    y_label           => '% FREE or USED',
    title             => $title,
   # '3d'                => 0,
    y_max_value       => 250,
    y_tick_number     => 50,
    y_label_skip      => 25,
    #label => $t2,
) or die $graph->error;
my @my_data = ( [ @legend ] , [ @data ] );
print "Dumper \@my_data\n";
print Dumper \@my_data;
my $gd = $graph->plot( \@my_data ) or die $graph->error;
# make PNG
$file_png = "CAP-".$file_title."-".$date.".png";
open(IMG, '>',$file_png) or die $!;
binmode IMG;
print IMG $gd->png;
close IMG;
print "Created file: $file_png\n";
push @files, $file_png;
 };
print "\nFinished with bar graph creation!!!\n\n";
if ( $array_count > 0 ) {
       return(\@files)
      } else {
      print "Only one Entry, returning single file: $file_png\n";
      return($file_png);
   }
};

###
sub graph_it_pies {
my($date,$data) = @_;
my $file_png;
my @files;
my $array_count = $#{ $data };
print "\$array_count: $array_count\n";
foreach my $ii ( 0 .. $#{ $data } ){
my $line = @{$data}[$ii];
print "\$line: $line\n";
my($title,$total,$free,$used) = split(/,/,$line);
my(@legend,@data);
my $file_title = $title;
push @legend,  $free." FREE";
push @legend, $used." USED";
push @data, $free;
push @data, $used;

use GD::Graph::pie;
use GD::Text;
my $graph = GD::Graph::pie->new(300,300);
$title = $title." Capacity Graph";
my $t2 = "Used vs. Free";
$graph->set(
   # x_label           => 'Samples taken over Time',
   # y_label           => 'GB\s Capacity Used on the Rubriks',
   # title             => 'CPU Capacity Graph',
    title             => $title,
    '3d'                => 0,
   # y_max_value       => 9000,
   # y_tick_number     => 500,
   # y_label_skip      => 50,
   label => $t2,
) or die $graph->error;
my @my_pie_data = ( [ @legend ] , [ @data ] );
print "Dumper \@my_pie_data\n";
print Dumper \@my_pie_data;

my $gd = $graph->plot( \@my_pie_data ) or die $graph->error;
# make PNG
$file_png = "CAP-".$file_title."-".$date.".png";
open(IMG, '>',$file_png) or die $!;
binmode IMG;
print IMG $gd->png;
close IMG;
print "Created file: $file_png\n";
push @files, $file_png;
 };
print "\nFinished with pie graph creation!!!\n\n";
if ( $array_count > 0 ) {
       return(\@files)
      } else {
      print "Only one Entry, returning single file: $file_png\n";
      return($file_png);
   }
};


sub graph_it_pie {
my($date,$legend,$data,$title) = @_;
my $file_png;
use GD::Graph::pie;
my $graph = GD::Graph::pie->new(500,300);
$graph->set(
   # x_label           => 'Samples taken over Time',
   # y_label           => 'GB\s Capacity Used on the Rubriks',
    title             => 'CPU Capacity Graph',
   # y_max_value       => 9000,
   # y_tick_number     => 500,
   # y_label_skip      => 50,
   label => 'CPU Capacity Graph',
) or die $graph->error;

#$graph->set_legend_font(GD::Font->Tiny);
#$graph->set_legend(@$legend);
#my @new_data_bucket = @$data;
#my @legend = @$legend;
my @my_pie_data = ( @{$legend},@{$data} );
print "Dumper \@my_pie_data\n";
print Dumper \@my_pie_data;
###exit;

my $gd = $graph->plot( \@my_pie_data ) or die $graph->error;
# make PNG
$file_png = "CAP-".$title."-".$date.".png";
open(IMG, '>',$file_png) or die $!;
binmode IMG;
print IMG $gd->png;
close IMG;
print "Created file: $file_png\n";
print "Finished with pie graph creation!!!\n";
return($file_png);
};

sub graph_it_line {
my($date,$legend,$data,$title) = @_;
use GD::Graph::hbars;
use GD::Graph::lines;
use GD::Graph::bars;
my $graph = GD::Graph::bars->new(400,200);
$graph->set(
   # x_label           => 'Samples taken over Time',
   # y_label           => 'GB\s Capacity Used on the Rubriks',
   # title             => 'CPU Capacity Graph',
    title => $title,
    y_max_value       => 250,
    y_tick_number     => 250,
    y_label_skip      => 50,
    overwrite => 2,
    label => $title,
) or die $graph->error;

#$graph->set_legend_font(GD::Font->Tiny);
#$graph->set_legend(@$legend);
#my @new_data_bucket = @$data;
#my @legend = @$legend;
my @data = ( @{$legend},@{$data} );
print "Dumper \@data\n";
print Dumper \@data;

my $gd = $graph->plot( \@data ) or die $graph->error;
# make PNG
my $file_png = "CAP-".$title."-".$date.".png";
open(IMG, '>',$file_png) or die $!;
binmode IMG;
print IMG $gd->png;
close IMG;
# make GIF
my $file_gif = "CAP-".$title."-".$date.".gif";
open(IMG, '>', $file_gif) or die $!;
binmode IMG;
print IMG $gd->gif;
close IMG;

print "finished with line graph creation!!!\n";
};

sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
$mon = sprintf("%02d", $mon);
$sec = sprintf("%02d", $sec);
$min = sprintf("%02d", $min);
$hour = sprintf("%02d", $hour);
my $date = $year."-".$mon."-".$mday."__".$hour."-".$min."-".$sec."_";
return ($date);
};

