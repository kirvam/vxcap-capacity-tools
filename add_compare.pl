use strict;
use warnings;
use Data::Dumper;
use POSIX;



my %hash_buffer = (
                    'DISK' => '39321600000000',
                    'CPU' => '716.8',
                    'MEMORY' => '1638400',
                    'USED' => '39321600000000',
                    'COUNT' => '0'
                  );

my %hash_cap = (
                  'DISK' => '196608000000000',
                  'COUNT' => '0',
                  'USED' => '196608000000000',
                  'MEMORY' => 8192000,
                  'CPU' => 3584
               ); 

my %hash_used = (
                   'DISK' => '207510282035509',
                   'COUNT' => '484',
                   'USED' => '119411997382325',
                   'MEMORY' => 3624832,
                   'CPU' => 1299
                );


my @AoH;

push @AoH, { %hash_used }; 
push @AoH, { %hash_buffer };
push @AoH, { %hash_cap };

my @array = qw ( USED OPS CAP );
my @order = qw ( COUNT CPU MEMORY DISK USED );


print "----< start $0 >----\n";
#print Dumper \@AoH;

my $AoH_ref = \@AoH;
my $string = print_totals($AoH_ref,\@array,\@order);
print "\n\nstring:\n$string\n";

my($string,$matrix_href,$aref) = print_totals_create_matrix($AoH_ref,\@array,\@order);

print "----< isanumber >----\n";
my($val) = isanumber('1.5');
print "\$val: $val\n";

print "----< add_these >----\n";
my @add_these = qw ( USED OPS );
my($total_ref,$plain_total_ref) = add_these(\@add_these,$matrix_href);



print "----< sort_hash_by_key >----\n";
my $string = sort_hash_by_key(\@order,$total_ref);
print "\n$string\n";

push @AoH, { %{ $plain_total_ref } };
my $AoH_ref = \@AoH;
print Dumper \$AoH_ref;
my @array = qw ( USED OPS CAP TOTAL-USED );
my $string = print_totals($AoH_ref,\@array,\@order);

print "----< end >-----\n\n\n";




sub sort_hash_by_key {
my($sort_aref,$href) = @_;
my $string;
foreach my $ii ( keys %{ $href } ){
 foreach my $item ( @{$sort_aref} ){
      #print "$item => ${$href}{$ii}{$item}\n";
      $string .= "$item => ${$href}{$ii}{$item}\n";
   };
 }
 return($string);
};

sub add_these {
my($aref,$href) = @_;
my %totals;
my %plain_totals;
my $total = "TOTAL-USED";
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  %{ ${$href}{$tmp} } ){
        print "${$href}{$tmp}{$ii}\n";
        $totals{$total}{$ii} += ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} += ${$href}{$tmp}->{$ii};
        
  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};

sub print_totals_create_matrix {
print "----< sub print_totals_create_matrix >----\n";
my $string;
my $key;
my %matrix;
my @AoA;
my($AoH_ref,$array_ref,$order_aref) = @_;
foreach my $item ( 0 .. $#{ $AoH_ref } ){
       #print "\$item: $item\n";
       print "$$array_ref[$item],";
       $string .= $$array_ref[$item].",";
       my @val;
       push @val, $$array_ref[$item];
        foreach $key ( @{$order_aref} ){
          #print "\$key: $key\n";  
          print "${$AoH_ref}[$item]->{$key},";
          $val = ${$AoH_ref}[$item]->{$key};
          $val = ceil($val);          
          $string .= $val.",";
          $matrix{$$array_ref[$item]}{$key} = $val;
          push @val, $val;
         }
         push @AoA, [ @val ];
         print "\n";
         $string .= "\n";    
    };
 print "----< \%matrix >----\n";
 print Dumper \%matrix;
 print Dumper \@AoA;
 return($string,\%matrix,\@AoA);
};
 
sub print_totals {
my $string;
my($AoH_ref,$array_ref,$order_aref) = @_;
foreach my $item ( 0 .. $#{ $AoH_ref } ){
       #print "\$item: $item\n";
       print "$$array_ref[$item],";
       $string .= $$array_ref[$item].",";
        #foreach my $key ( sort keys ${$AoH_ref}[$item] ){
        foreach my $key ( @{$order_aref} ){
          #print "\$key: $key\n";  
          print "${$AoH_ref}[$item]->{$key},";
          my $val = ${$AoH_ref}[$item]->{$key};
          $val = ceil($val);          
          #$string .= ${$AoH_ref}[$item]->{$key}.",";
          $string .= $val.",";
         }
         print "\n";
         $string .= "\n";    
    };
 return($string);
};

###  BROKE ###
sub isanumber {
my($string) = @_;
print "Original value: $string\n";
if ( $string =~ /^(\d+\.?\d*)$/ ) {
        $string = ceil($string);       
        print "$string is a number.\n";
        } else {
         print "$string NOT a number.\n";
    }
return($string);
};

###
