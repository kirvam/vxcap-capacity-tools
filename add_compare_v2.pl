use strict;
use warnings;
use Data::Dumper;
use POSIX;
use GD::Dashboard;

my %hash_buffer = (

        'OPS' =>  {
                    'DISK' => '39321600000000',
                    'CPU' => '716.8',
                    'MEMORY' => '1638400',
                    'USED' => '39321600000000',
                    'COUNT' => '0'
                  }
              );

my %hash_cap = (
 
       'CAP' => {  
                  'DISK' => '196608000000000',
                  'COUNT' => '0',
                  'USED' => '196608000000000',
                  'MEMORY' => 8192000,
                  'CPU' => 3584
               } 
             );

my %hash_used = (

         'USED' => { 
                    'DISK' => '207510282035509',
                    'COUNT' => '484',
                    'USED' => '119411997382325',
                    'MEMORY' => 3624832,
                    'CPU' => 1299
                }
           );

my @type_order = qw ( USED OPS CAP );
my @comp_order = qw ( COUNT CPU MEMORY DISK USED );

print "----< start $0 >----\n";
my %matrix;
my($main_href) = add_to_hash(\%matrix,\%hash_used);

print "----< isanumber >----\n";
my($val) = isanumber('1.5');
print "\$val: $val\n";
print "\n";

my($main_href) = add_to_hash(\%matrix,\%hash_buffer);
my($main_href) = add_to_hash(\%matrix,\%hash_cap);


my @type_order = qw ( USED OPS );
my($string) = sort_hash_by_key(\@type_order,\%matrix);
my @type_order = qw ( OPS USED );
my($string) = sort_hash_by_key(\@type_order,\%matrix);

my @add_these = qw ( USED OPS );
my($total_ref,$plain_total_ref) = add_these(\@add_these,\%matrix,'TOTALUSED');

my($main_href) = add_to_hash(\%matrix,$total_ref);

print "Dumper \%matrix\n";
print Dumper \%matrix;


my @type_order = qw ( OPS USED TOTALUSED );
my($string) = sort_hash_by_key(\@type_order,\%matrix);

print "Dumper \%matrix\n";
print Dumper \%matrix;
#########################################exit;

my %main = %matrix;
my @add_these = qw ( CAP TOTALUSED );
my($total_delta_ref,$plain_delta_total_ref) = minus_these_now(\@add_these,\%matrix,'DELTA');
my($main_href) = add_to_hash(\%main,$total_delta_ref);
print "Dumper \$main_href  MAIN\n";
print Dumper \$main_href;
#exit;


### PERCENTAGES
my @percent_these = qw ( CAP TOTALUSED );
print "Dumper \@percent_these\n";
print Dumper @percent_these;

my($total_delta_ref,$plain_delta_total_ref) = mk_percent(\@percent_these,\%matrix,'PERCENTAGE');
my($main_href) = add_to_hash_no_ceil(\%main,$total_delta_ref);
print "Dumper \$main_href  MAIN\n";
print Dumper \$main_href;




print "Dashboard\n";
dashboard();

print "----< end >-----\n\n\n";
###
sub mk_percent {
print "-------< percent these >-----\n";
my($aref,$href,$title) = @_;
my %totals;
my %plain_totals;
if ( ! $title ) { $title = "PERCENT" };
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  ${$href}{$tmp} ){
         print "^$ii^ => ${$href}{$tmp}{$ii}\n";
         if( $ii =~ /^COUNT$/i ){ print "### matches COUNT!!!\n"; };
        if( $tag ge 1 && $ii !~ m/COUNT/ ){
        print "$totals{$title}{$ii} eq ${$href}{$tmp}->{$ii} div $totals{$title}{$ii}\n";
        $totals{$title}{$ii} = int ( ${$href}{$tmp}->{$ii}) / int ($totals{$title}{$ii});
        $plain_totals{$ii} = int ( ${$href}{$tmp}->{$ii}) / int ($plain_totals{$ii} );
          print "=> $totals{$title}{$ii}\n";
          } else {
        $totals{$title}{$ii} = int(${$href}{$tmp}->{$ii});
        $plain_totals{$ii} = int(${$href}{$tmp}->{$ii});
      };
  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print "Dumper \%plain_totals\n";
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};

sub minus_these_now {
print "-------< minus these now >-----\n";
my($aref,$href,$title) = @_;
my %totals;
my %plain_totals;
if ( ! $title ) { $title = "DELTA" };
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  ${$href}{$tmp} ){
         print "^$ii^ => ${$href}{$tmp}{$ii}\n";
         if( $ii =~ /^COUNT$/i ){ print "### matches COUNT!!!\n"; };
        if( $tag ge 1 && $ii !~ m/COUNT/ ){
        $totals{$title}{$ii} -= ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} -= ${$href}{$tmp}->{$ii};
          } else {
        $totals{$title}{$ii} = ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} = ${$href}{$tmp}->{$ii};
      };
  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print "Dumper \%plain_totals\n";
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};

###
sub add_to_hash_no_ceil {
my($main_href,$add_href) = @_;
foreach my $key ( keys %{ $add_href } ){
       print $key,"\n";
        foreach my $ii ( keys ${$add_href}{$key} ){
           print $ii," => ";
           print ${$add_href}{$key}{$ii},"\n";
           my $val = ${$add_href}{$key}{$ii};
           # $val = ceil($val);
           ${$main_href}{$key}{$ii} = $val;
          }
    };
 print Dumper \$main_href;
 return(\$main_href);
};

sub add_to_hash {
my($main_href,$add_href) = @_;
foreach my $key ( keys %{ $add_href } ){
       print $key,"\n";
        foreach my $ii ( keys ${$add_href}{$key} ){
           print $ii," => ";
           print ${$add_href}{$key}{$ii},"\n";
           my $val = ${$add_href}{$key}{$ii};
           $val = ceil($val);
           ${$main_href}{$key}{$ii} = $val;
          }
    };
 print Dumper \$main_href;
 return(\$main_href);
};




sub sort_hash_by_key {
my($sort_aref,$href) = @_;
my $string;
foreach my $key ( 0 .. $#{ $sort_aref } ){
      print ${$sort_aref}[$key],"\n";
      $string .= ${$sort_aref}[$key].",";
 foreach my $item ( keys ${$href}{${$sort_aref}[$key]} ){
      print "$item => ${$href}{${$sort_aref}[$key]}{$item}\n";
      $string .= "${$href}{${$sort_aref}[$key]}{$item},";
   };
      print "\n";
      $string .= "\n";
 }
 print "\n$string\n";
 return($string);
};

sub minus_these {
my($aref,$href,$title) = @_;
my %totals;
my %plain_totals;
if ( ! $title ) { $title = "TOTALSUSED" };
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  %{ ${$href}{$tmp} } ){
        print "$ii => ${$href}{$tmp}{$ii}\n";
        print "#### $totals{$title}{$ii} minus ${$href}{$tmp}->{$ii} eq $totals{$title}{$ii}\n";
        $totals{$title}{$ii} -= ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} -= ${$href}{$tmp}->{$ii};
        
  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print "Dumper \%plain_totals\n";
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};



sub add_these {
my($aref,$href,$title) = @_;
my %totals;
my %plain_totals;
if ( ! $title ) { $title = "TOTAL-USED" };
foreach my $tag ( 0 .. $#{ $aref } ){
    print "\$tag: ${$aref}[$tag]\n";
    my $tmp = ${$aref}[$tag];
    foreach my $ii ( keys  %{ ${$href}{$tmp} } ){
        print "$ii => ${$href}{$tmp}{$ii}\n";
        $totals{$title}{$ii} += ${$href}{$tmp}->{$ii};
        $plain_totals{$ii} += ${$href}{$tmp}->{$ii};
        
  };
 };
 print "Dumper \%totals\n";
 print Dumper \%totals;
 print "Dumper \%plain_totals\n";
 print Dumper \%plain_totals;
 return(\%totals,\%plain_totals);
};


sub print_totals_create_matrix {
print "----< sub print_totals_create_matrix >----\n";
my $string;
my $key;
my $tag;
my %matrix;
my %master;
my @AoA;
my($AoH_ref,$type_aref,$comp_aref) = @_;
#print "Dumper: \$AoH_ref\n";
print Dumper \$AoH_ref;
#print "### ${$AoH_ref}[0]{'USED'}->{'DISK'}\n";
 my $jj = 0;
        #print "$jj\n";
foreach my $kk ( 0 .. $#{ $type_aref } ){
         my $key = "${$type_aref}[$kk]";
         print "$key\n";
          my $line = "$key,";
          $string .= $key.",";
         foreach my $ii ( 0 .. $#{ $comp_aref } ){
           my $tag = "${$comp_aref}[$ii]";
            #print "$tag\n";
             print "^${$AoH_ref}[$jj]{$key}{$tag}^,";
              my $val = ${$AoH_ref}[$jj]{$key}{$tag};
              $val = ceil($val);
              $master{$key}{$tag} = $val;
              $line .= "$val,";
              $string .= "$val,";
          };
        $matrix{$key} = $line;
        push @AoA, [ $line ];
        print "\n";
        $string .= "\n";
        $jj++;
     };
  print "\n\n$string\n";

 print "----< \%matrix >----\n";
  print Dumper \%matrix;
  print Dumper \@AoA;
  print Dumper \%master;
  return($string,\%matrix,\@AoA,\%master);
};

 
sub print_totals {
my $string;
my($AoH_ref,$array_ref,$order_aref) = @_;
foreach my $item ( 0 .. $#{ $AoH_ref } ){
       #print "\$item: $item\n";
       print "$$array_ref[$item],";
       $string .= $$array_ref[$item].",";
        #foreach my $key ( sort keys ${$AoH_ref}[$item] ){
        foreach my $key ( @{$order_aref} ){
          #print "\$key: $key\n";  
          print "${$AoH_ref}[$item]->{$key},";
          my $val = ${$AoH_ref}[$item]->{$key};
          $val = ceil($val);          
          #$string .= ${$AoH_ref}[$item]->{$key}.",";
          $string .= $val.",";
         }
         print "\n";
         $string .= "\n";    
    };
 return($string);
};

sub isanumber {
my($string) = @_;
print "---<isanumber>---\nOriginal value: $string\n";
if ( $string =~ /^(\d+\.?\d*)$/ ) {
        $string = ceil($string);       
        print "$string is a number.\n";
        } else {
         print "$string NOT a number.\n";
    }
print "---<isanumber>---\n";
return($string);
};


sub dashboard {

my $dash = new GD::Dashboard( FNAME=>'m1.jpg'); 
#my $dash = new GD::Dashboard(); 

my($empcnt)= 100;
my($nopwp_cnt) = 123; 

#my $g1 = new GD::Dashboard( FNAME=>'m1.jpg');
my $g1 = new GD::Dashboard::Gauge(
                   MIN=>0,
                   MAX=>$empcnt,
                   VAL=>$nopwp_cnt,
                   NA1=>3.14/2+0.85,
                   NA2=>3.14/2-0.85,
                   NX=>51,NY=>77,NLEN=>50                      
         );

my($empcnt)= 100;
my($nopwp_cnt) = 77; 

#my $g2 = new GD::Dashboard( FNAME=>'m1.jpg');
my $g2 = new GD::Dashboard::Gauge( 
                   MIN=>0,
                   MAX=>$empcnt,
                   VAL=>$nopwp_cnt,
                   NA1=>3.14/2+0.85,
                   NA2=>3.14/2-0.85,
                   NX=>51,NY=>77,NLEN=>50                      
         );


 
$dash->add_meter('DISK', $g1);
$dash->add_meter('CPU', $g2);

$dash->write_jpeg('dash1.jpg');

};

